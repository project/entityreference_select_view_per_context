<?php

/**
 * @file
 * Contains entityreference selection plugin definition.
 */

$plugin = array(
  'title' => t('View Per Context: Filter by view selected per context'),
  'class' => 'EntityReferenceSelectViewPerContext',
  'weight' => 0,
);
